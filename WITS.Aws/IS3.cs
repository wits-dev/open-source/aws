﻿using System;
using System.IO;
using System.Threading.Tasks;
using WITS.Aws.Option;

namespace WITS.Aws
{
    public interface IS3
    {
        Task<bool> ObjectExists(string bucket, string objectKey);

        Task<bool> PutObject(string bucket, string objectKey, Stream stream, bool overwrite = true, string contentType = null, bool isPublic = false, StorageClass storageClass = StorageClass.Standard);

        Task<bool> DeleteObject(string bucket, string objectKey);

        string GetPresignedUrl(string bucket, string objectKey, DateTime expiresAt, string contentType = null, string filename = null, Protocol protocol = Protocol.Https);

        string GetObjectUrl(string bucket, string key, Protocol protocol = Protocol.Https);

        string ExtensionToContentType(string fileExtension);
    }
}