﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.IO;
using System.Threading.Tasks;
using WITS.Aws.Option;

namespace WITS.Aws
{
    public class S3 : IS3
    {
        private readonly AmazonS3Client s3Client;// = new AmazonS3Client(credentials, Amazon.RegionEndpoint.EUWest1);

        public S3(AmazonS3Client s3Client)
        {
            this.s3Client = s3Client;
        }

        /// <summary>
        /// Save an object to S3 storage
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="objectKey"></param>
        /// <param name="stream"></param>
        /// <param name="overwrite"></param>
        /// <param name="contentType"></param>
        /// <param name="storageClass">
        /// Standard - most durable
        /// StandardInfrequentAccess - cheapest but has implications if you overwrite, move, or delete before 30 days
        /// ReducedRedundancy - in between but lower durability guranteed
        /// </param>
        /// <returns>boolean to indicate write/overwrite. throws to indicate failure</returns>
        public async Task<bool> PutObject(string bucket, string objectKey, Stream stream, bool overwrite = true, string contentType = null, bool isPublic = false, StorageClass storageClass = StorageClass.Standard)
        {
            try
            {
                PutObjectResponse response = default(PutObjectResponse);

                PutObjectRequest objReq = new PutObjectRequest
                {
                    Key = objectKey,
                    BucketName = bucket,
                    CannedACL = isPublic ? S3CannedACL.PublicRead : S3CannedACL.Private,
                    InputStream = stream,
                    AutoCloseStream = true,
                    StorageClass = storageClass == StorageClass.ReducedRedundancy ? S3StorageClass.ReducedRedundancy
                                    : storageClass == StorageClass.StandardInfrequentAccess ? S3StorageClass.StandardInfrequentAccess
                                    : S3StorageClass.Standard,
                };

                if (contentType != null)
                    objReq.ContentType = contentType;

                if (overwrite || !await ObjectExists(bucket, objectKey))
                    response = await s3Client.PutObjectAsync(objReq);
                else
                    return false;

                if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                    throw new AmazonS3Exception($"PutObject: {bucket}: {objectKey}: HTTP {response.HttpStatusCode}");

                return true;
            }
            catch (AmazonS3Exception e)
            {
                throw new AmazonS3Exception($"PutObject: {bucket}: {objectKey}: {e.Message}", e);
            }
        }
        
        public async Task<bool> DeleteObject(string bucket, string objectKey)
        {
            try
            {
                DeleteObjectResponse response = default(DeleteObjectResponse);

                DeleteObjectRequest objReq = new DeleteObjectRequest
                {
                    Key = objectKey,
                    BucketName = bucket,
                };
                
                response = await s3Client.DeleteObjectAsync(objReq);
                
                if (response.HttpStatusCode != System.Net.HttpStatusCode.NoContent)
                    throw new AmazonS3Exception($"DeleteObject: {bucket}: {objectKey}: HTTP {response.HttpStatusCode}");

                return true;
            }
            catch (AmazonS3Exception e)
            {
                throw new AmazonS3Exception($"DeleteObject: {bucket}: {objectKey}: {e.Message}", e);
            }
        }

        public string GetPresignedUrl(string bucket, string objectKey, DateTime expiresAt, string contentType = null, string filename = null, Option.Protocol protocol = Option.Protocol.Https)
        {
            try
            {
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest()
                {
                    BucketName = bucket,
                    Key = objectKey,
                    Protocol = (Amazon.S3.Protocol)protocol,
                    Expires = expiresAt
                };

                if (contentType != null)
                    request.ResponseHeaderOverrides.ContentType = contentType;
                if (filename != null)
                    request.ResponseHeaderOverrides.ContentDisposition = $"attachment; filename={filename}";

                return s3Client.GetPreSignedURL(request);
            }
            catch (AmazonS3Exception e)
            {
                throw new AmazonS3Exception($"GetPresignedUrl: {bucket}: {objectKey}: {e.Message}", e);
            }
        }

        public string GetObjectUrl(string bucket, string key, Option.Protocol protocol = Option.Protocol.Https)
        {
            var _protocol = protocol == Option.Protocol.Https ? "https" : "http";
            var url = $"{_protocol}://{bucket}.s3.amazonaws.com/{key.TrimStart('/')}";

            return url;
        }

        public async Task<bool> ObjectExists(string bucket, string key)
        {
            try
            {
                await s3Client.GetObjectMetadataAsync(new GetObjectMetadataRequest()
                {
                    BucketName = bucket,
                    Key = key,
                }).ConfigureAwait(false);

                return true;
            }
            catch (AmazonS3Exception e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                throw new AmazonS3Exception($"ObjectExists: {bucket}: {key}: {e.Message}", e);
            }
        }

        public string ExtensionToContentType(string fileExtension)
        {
            string contentType = "application/octet-stream";
            switch (fileExtension.Trim('.'))
            {
                case "bmp":
                    contentType = "image/bmp";
                    break;

                case "jpeg":
                case "jpg":
                    contentType = "image/jpeg";
                    break;

                case "gif":
                    contentType = "image/gif";
                    break;

                case "tiff":
                    contentType = "image/tiff";
                    break;

                case "png":
                    contentType = "image/png";
                    break;

                case "txt":
                    contentType = "text/plain";
                    break;

                case "rtf":
                    contentType = "text/rtf";
                    break;

                case "dot":
                case "doc":
                    contentType = "application/msword";
                    break;

                case "docx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;

                case "xls":
                    contentType = "application/vnd.ms-excel";
                    break;

                case "xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;

                case "ppt":
                    contentType = "application/vnd.ms-powerpoint";
                    break;

                case "pptx":
                    contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    break;

                case "zip":
                    contentType = "application/zip";
                    break;

                case "mp3":
                    contentType = "audio/mpeg";
                    break;

                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "gzip":
                    contentType = "application/x-gzip";
                    break;
            }
            return contentType;
        }
    }
}