﻿namespace WITS.Aws.Option
{
    public enum StorageClass
    {
        ReducedRedundancy,
        StandardInfrequentAccess,
        Standard
    }
}