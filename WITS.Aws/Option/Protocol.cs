﻿namespace WITS.Aws.Option
{
    public enum Protocol
    {
        Https = 0,
        Http = 1
    }
}